# devops-netology
Репозиторий для заданий курса "DevOps и системное администрирование"

Студент: Иван Жиляев

# Домашнее задание к занятию «2.1. Системы контроля версий.»

## Задание №1 – Создать и настроить репозиторий для дальнейшей работы на курсе.

Файлы, которые будут проигнорированы в будущем благодаря 
добавленному [terraform/.gitignore](terraform/.gitignore):

1. __\*\*/.terraform/\*__

   + единственное правило из всех, которое относится к папкам - все остальные
   пункты относятся к файлам
   
   + правило действует на любом уровне вложенности от файла [terraform/.gitignore](terraform/.gitignore)
   
   + исключаются папки `.terraform`, например:
      + `terraform/.terraform/`
      + `terraform/some_folder/.terraform/` и т.д.  
      

   >Считаю, что символ `*` в конце правила формально излишен и достаточно обозначить то,
   что это папка окончив правило слэшем `/`. Возможно, это сделано для наглядности.

1. __\*.tfstate__ и __\*.tfstate.\*__

   + как и последущие правила, это правило действует на любом уровне вложенности от файла `.gitignore`
   
   + любые файлы, оканчивающиеся на `.tfstate`, например:
      + `.tfstate`
      + `best.tfstate`
      + `state/last.tfstate`

1. __crash.log__

   + все файлы `crash.log`, например:
      + `crash.log`
      + `build/crash.log`
      + `archive/wasted_2017/march/17/crash.log`
      
1. __\*.tfvars__

   + любые файлы, оканчивающиеся на `.tfvars`, например:
      + `707_values.tfvars`
      + `variables/.tfvars`
      
1. __override.tf__, __override.tf.json__, __\*\_override.tf__ и __\*\_override.tf.json__

   + файлы `override.tf` и `override.tf.json`
      + `one/override.tf`
      + `two/jsons/override.tf.json`
   
   + плюс файлы с такими же базовыми именами и префиксами, оканчивающимися на underscore `_`
      + `three/what_a_long_path/here_it_is_override.tf`
      + `simple/_override.tf.json`
   
1. __.terraformrc__ и __terraform.rc__

   + фильтруются сами файлы `.terraformrc` и `terraform.rc`
      + `hidden_files/.terraformrc`
      + `terraform.rc`
